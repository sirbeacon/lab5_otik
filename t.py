import struct

value = 991828

with open('t', 'wb') as f:
    f.write(struct.pack('i', value))

with open('t', 'rb') as fin:
    print(struct.unpack('i', fin.read(4))[0])
