import contextlib, sys, struct
import arithmeticcoding
python3 = sys.version_info.major >= 3


signature = 991828

def main(args):
	# Handle command line arguments
	if len(args) != 2:
		sys.exit("Usage: python arithmetic-compress.py InputFile OutputFile")
	inputfile, outputfile = args
	
	# Read input file once to compute symbol frequencies
	freqs = get_frequencies(inputfile)
	freqs.increment(256)  # EOF symbol gets a frequency of 1
	
	# Read input file again, compress with arithmetic coding, and write output file
	with open(inputfile, "rb") as inp, open(outputfile, "wb") as out, \
			contextlib.closing(arithmeticcoding.BitOutputStream(out)) as bitout:
		out.write(struct.pack('i', signature))
		write_frequencies(bitout, freqs)
		compress(freqs, inp, bitout)


def get_frequencies(filepath):
	freqs = arithmeticcoding.SimpleFrequencyTable([0] * 257)
	with open(filepath, "rb") as input:
		while True:
			b = input.read(1)
			if len(b) == 0:
				break
			b = b[0] if python3 else ord(b)
			freqs.increment(b)
	return freqs


def write_frequencies(bitout, freqs):
	for i in range(256):
		write_int(bitout, 32, freqs.get(i))


def compress(freqs, inp, bitout):
	enc = arithmeticcoding.ArithmeticEncoder(32, bitout)
	while True:
		symbol = inp.read(1)
		if len(symbol) == 0:
			break
		symbol = symbol[0] if python3 else ord(symbol)
		enc.write(freqs, symbol)
	enc.write(freqs, 256)  # EOF
	enc.finish()  # Flush remaining code bits


def write_int(bitout, numbits, value):
	for i in reversed(range(numbits)):
		bitout.write((value >> i) & 1)  # Big endian


# Main launcher
if __name__ == "__main__":
	main(sys.argv[1 : ])
